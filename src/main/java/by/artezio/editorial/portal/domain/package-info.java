/**
 *   В пакете by.artezio.editorial.portal.domain должны распологаться классы-сущности,
 *   которые содержат ORM маппинг и енамы.
 */
package by.artezio.editorial.portal.domain;
