/**
 * В пакете by.artezio.editorial.portal.service должны распологаться классы,
 * реализующие бизнес-логику и относящиеся к слою сервисов.
 */
package by.artezio.editorial.portal.service;
