/**
 *   В пакете by.artezio.editorial.portal.dao должны распологаться классы,
 *   предоставляющие доступ к данным в БД.
 */
package by.artezio.editorial.portal.dao;
