/**
 *   В пакете by.artezio.editorial.portal.web.controllers должны распологаться
 *   контроллеры, возвращающие пользователям представления(наши jsp вьюхи) с данными.
 */
package by.artezio.editorial.portal.web.controllers;
