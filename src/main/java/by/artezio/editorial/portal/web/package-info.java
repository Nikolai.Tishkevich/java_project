/**
 *   В пакете by.artezio.editorial.portal.web.rest должны распологаться контроллеры,
 *   реализующие REST сервисы, и контроллеры, возвращающие пользователю представления
 *   (jsp вьюшки).
 */
package by.artezio.editorial.portal.web;
